<?php

function ChargerClasse($class) {
    if(file_exists("../class/". $class . ".php")) {
        require "../class/". $class . ".php";
    } elseif (file_exists("../repository/". $class . ".php")) {
        require "../repository/". $class . ".php";
    } else {
        exit("Le fichier $class.php n'existe ni dans class ni dans repository.");

    }
}

spl_autoload_register("ChargerClasse");

session_start()


?>