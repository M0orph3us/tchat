<?php
require "../init.php";
$pseudo = $_SESSION["pseudo"];

if($_SESSION["pseudo"] === null) {
  header("location:index.php");
}

if (isset($_POST["message"]) && !empty($_POST["message"])) {
  $message = new MessagesRepository();
  $new = $message->newMessage(htmlentities($_POST["message"]), htmlentities($_SESSION["pseudo"]));
}

$getMessages = new MessagesRepository();
$Messages = $getMessages->getMessages(htmlentities($pseudo));

// $pseudos = new UsersRepository();
// $username = $pseudos->readPseudo($Messages['message']);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/style.css">
  <title>Tchat</title>
</head>

<body>
  <div id="conteneur" class="conteneur">
    <div id="messages" class="messages">
      <?php
      foreach ($Messages as $message) { ?>
        <p class="M_date"><?= $message["sending_date"] ?></p>
        <div id="m12" class="M M_envoye">
          <h3><?= $pseudo ?></h3>
          <p class="M_message"><?= $message["message"] ?></p>
        </div>
      <?php } ?>
      <p class="M_date">18/02/2023</p>
      <p class="M_date">10h12</p>
      <div id="m12" class="M M_recu">
        <h3>Anne</h3>
        <p class="M_message">Regarde comme c'est beau !</p>
      </div>
    </div>
    <div id="post" class="post">
      <form action="#" method="POST">
        <textarea name="message" id="message" cols="30" rows="5" placeholder="votre message" class="new_message"></textarea>
        <input type="submit" name="newPost" value="Envoyer" class="new_submit">
      </form>
    </div>
    <a href="deco.php">Deconnexion</a>

    <!-- <script>
      const sub = document.querySelector("");
      
      function mess() {
      const h1 = document.createElement("h1");
      let message = document.querySelector('#message').value
      h1.innerHTML = `<p>${message}</p>`;
      return h1;
      }
    </script> -->
</body>

</html>