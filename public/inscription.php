<?php
require "../init.php";

if (isset($_POST["pseudo"], $_POST["password"]) && !empty($_POST["pseudo"]) && !empty($_POST["password"])) {
    $user = new UsersRepository();
    $new = $user->createUser($_POST["pseudo"], $_POST["password"]);

    if ($new === false) {
        header("location:index.php");
    }
}


?>

<h1>Inscription</h1>
<form style="display:flex; flex-direction:column; width:200px; margin-left:20px" action="#" method="POST">
    <label for="pseudo">Nom d'utilisateur : </label>
    <input type="text" name="pseudo" required>
    <label for="password">Mot de Passe : </label>
    <input type="text" name="password" required>
    <button type="submit" style="margin-top:15px;">S'incrire</button>
</form>