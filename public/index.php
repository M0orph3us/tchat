<?php
require "../init.php";

if (isset($_POST["pseudo"], $_POST["password"]) && !empty($_POST["pseudo"]) && !empty($_POST["password"])) {
    $_SESSION["pseudo"] = $_POST["pseudo"];
    $user = new UsersRepository();
    $new = $user->login($_POST["pseudo"], $_POST["password"]);

    if(!empty($new))
    {
        header("location:tchat.php");
    }
}


?>

<h1>Connexion</h1>
<form style="display:flex; flex-direction:column; width:200px; margin-left:20px" action="#" method="POST">
    <label for="pseudo">Nom d'utilisateur : </label>
    <input type="text" name="pseudo" required>
    <label for="password">Mot de Passe : </label>
    <input type="text" name="password" required>
    <button style="margin-top:15px;">Connexion</button>
    <a href="inscription.php">S'incrire</a>
</form>