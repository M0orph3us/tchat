<?php
class Messages {
    // Les Attributs
    private $_id;
    private $_message;
    private $_sending_date;
    private $_id_User;

    // Constructor
    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    // Method
    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters/Setters
    public function getId()
    {
        return $this->_id;
    }
    
    private function setId(int $id)
    {
        $this->_id = $id;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    private function setMesage(string $message)
    {
        $this->_message = htmlentities($message);
    }

    public function getDate()
    {
        return $this->_sending_date;
    }

    private function setDate(string $date)
    {
        $sending_date = date("d/m/y", $date);
        $this->_sending_date = $sending_date;
    }

    public function getId_User()
    {
       return $this->_id_User;
    }
}
