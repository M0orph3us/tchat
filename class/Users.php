<?php
class Users {
    // Les Attributs
    private $_id;
    private $_pseudo;
    private $_password;
    
    // Constructor
    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    // Method
    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters/Setters
    public function getId()
    {
        return $this->_id;
    }

    private function setId(int $id)
    {
        $this->_id = $id;
    }

    public function getPseudo()
    {
        return $this->_pseudo;
    }

    private function setPseudo(string $pseudo)
    {
        $this->_pseudo = htmlentities($pseudo);
    }

    public function getPassword()
    {
        return $this->_password;
    }

    private function setPassword(string $password)
    {
        $this->_password = password_hash(htmlentities($password), PASSWORD_DEFAULT);
    }
}
