<?php
class Lire {
    // Le Attributs
    private $_id_message;
    private $_id_User;

    // Constructor
    public function __construct(Array $infos)
    {
        $this->hydrate($infos);
    }

    // Les Méthodes
    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters/Setters
    public function getId_User()
    {
        return $this->_id_User;
    }

    private function setId_User($id_User)
    {
        $this->_id_User = $id_User;
    }

    public function getId_Message()
    {
        return $this->_id_message;
    }

    private function setId_Message($id_Message)
    {
        $this->_id_message = $id_Message;
    }
}

?>