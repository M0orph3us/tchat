#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: User
#------------------------------------------------------------

CREATE TABLE User(
        id       Int  Auto_increment  NOT NULL ,
        pseudo   Varchar (100) NOT NULL ,
        password Varchar (100) NOT NULL
	,CONSTRAINT User_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Message
#------------------------------------------------------------

CREATE TABLE Message(
        id           Int  Auto_increment  NOT NULL ,
        message      Longtext NOT NULL ,
        sending_date Date NOT NULL ,
        id_User      Int NOT NULL
	,CONSTRAINT Message_PK PRIMARY KEY (id)

	,CONSTRAINT Message_User_FK FOREIGN KEY (id_User) REFERENCES User(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Lire
#------------------------------------------------------------

CREATE TABLE Lire(
        id_Message      Int NOT NULL ,
        id_User Int NOT NULL
	,CONSTRAINT Lire_PK PRIMARY KEY (id_Message,id_User)

	,CONSTRAINT Lire_Message_FK FOREIGN KEY (id_Message) REFERENCES Message(id)
	,CONSTRAINT Lire_User0_FK FOREIGN KEY (id_User) REFERENCES User(id)
)ENGINE=InnoDB;

