<?php

class Database {
  const DB_HOST = "localhost";
  const DB_BASE = "tchat";
  const DB_USER = "tchat";
  const DB_MDP = "tchat";

  private $_BDD;

  public function __construct()
  {
    $this->connectBDD();
  }

  private function connectBDD()
  {
    try {
      $this->_BDD = new PDO(
        "mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_BASE,
        self::DB_USER,
        self::DB_MDP,
        [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]
      );
    } catch (PDOException $e) {
      die("Erreur de connexion : " . $e->getMessage());
    }
  }

  public function getBDD()
  {
    return $this->_BDD;
  }

  public function closeBDD()
  {
    $this->_BDD = null;
  }
}
