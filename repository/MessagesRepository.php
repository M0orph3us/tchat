<?php

class MessagesRepository{
  // Les Attributs
  private $_db;

  public function __construct()
  {
    $this->_db = new Database();
    $this->_db = $this->_db->getBDD();
  }
                  // Le CRUD

  // Creation du Message
  public function newMessage(string $message, $pseudo_User)
  {

    $sql = "INSERT INTO messages (message, sending_date, id_User) VALUES (:message, NOW(),(SELECT id FROM users WHERE pseudo = :pseudo_User));";

    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute([
        ':message' => $message,
        ':pseudo_User' => $pseudo_User
      ]);
    } catch (PDOException $e) {
      echo "erreur d'insertion de message : " . $e->getMessage();
    }
  }

  // Afficher les Messages pour l'utilisateur connecté
  public function getMessages($pseudo)
  {
    $sql = "SELECT * FROM messages WHERE id_User = (
      SELECT id FROM users WHERE pseudo = :pseudo) ORDER BY id asc;";
    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute([
        ':pseudo' => $pseudo
      ]);
      $resultat = $requete->fetchAll();
    } catch (PDOException $e) {
      echo "erreur de recupération des messages : " . $e->getMessage();
    }
    return $resultat;
  }

  // Afficher tous Messages
  public function getAllMessages()
  {
    $sql = "SELECT * FROM messages;";
    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute();
      $resultat = $requete->fetchAll();
    } catch (PDOException $e) {
      echo "erreur de recupération des messages : " . $e->getMessage();
    }
    return $resultat;
  }


  // Modifier le Message

  public function updateMessage()
  {
  }

  // Supprimer le Message

  public function deleteMessage()
  {
  }
}
