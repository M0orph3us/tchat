<?php

class UsersRepository {

  private $_db;

  public function __construct()
  {
    $this->_db = new Database();
    $this->_db = $this->_db->getBDD();
  }
                  // Le CRUD

  // Creation User
  public function createUser(string $pseudo, string $mdp)
  {

    $sql = "INSERT INTO users (pseudo, password) VALUES (:pseudo, :password);";
    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute([
        ':pseudo' => htmlentities($pseudo),
        ':password' => password_hash(htmlentities($mdp), PASSWORD_DEFAULT)
      ]);
    } catch (PDOException $e) {
      echo "erreur d'inscription : " . $e->getMessage();
    }
    if(!empty($resultat)){
      return true;
    } else {
      return false;
    }
  }
  // Lecture des données de l'user

  public function readUser(string $pseudo)
  {
    $sql = "SELECT * FROM users WHERE pseudo = :pseudoUser;";
    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute([
      ':pseudoUser' => $pseudo
    ]);
    } catch (PDOException $e) {
      echo "erreur de login : " . $e->getMessage();
    }

    $resultat = $requete->fetch();
    return $resultat;
  }

  public function readPseudo(string $message) {
    $sql = "SELECT pseudo FROM users WHERE id = (
      SELECT id_User FROM messages WHERE message = :message;";

      try {
        $requete = $this->_db->prepare($sql);
        $requete->execute([
        ':message' => $message
      ]);
      $resultat = $requete->fetch();
      } catch (PDOException $e) {
        echo "erreur de pseudo: " . $e->getMessage();
      }
      return $resultat['pseudo'];
  }
  
  

  // MAJ des données de l'user

  public function updateUser()
  {
    // $sql = "UPDATE ;";
  }
  // Suppression de l'utilisateur

  public function deleteUser(int $idUser)
  {
    
  }
  //  Les Méthodes
  
  public function login(string $pseudo, string $mdp)
  {

    $sql = "SELECT * FROM users  WHERE pseudo = :pseudo;";
    try {
      $requete = $this->_db->prepare($sql);
      $requete->execute([
        ":pseudo" => htmlentities($pseudo)
      ]);
      $resultat = $requete->fetch(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      echo "erreur de login : " . $e->getMessage();
    }

    if (password_verify(htmlentities($mdp), $resultat->password)) {
      return $resultat;
    } else {
      return false;
    }
  }
}

?>
